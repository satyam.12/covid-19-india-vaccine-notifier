'''
Script: Covid Vaccine Slot Availability Notifier
By SYKik PikaChu
'''

from datetime import datetime, timedelta
from configparser import ConfigParser
from plyer import notification
from apscheduler.schedulers.blocking import BlockingScheduler
import requests

# Constants
HEADERS = {
    'User-Agent': 
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.76 Safari/537.36'
    } 

FLAG = False


# configurations
config = ConfigParser()
config.read('config.ini')
scheduler = BlockingScheduler()


# function to show notification
def notify(head, body):
    notification.notify(
        title=head,
        message=body,
        timeout=int(config['Timer']['msg_duration']),
        app_icon=config['Source']['icon_path']
    )


# function to get the data from URL
def get_url_data(url):
    response = requests.get(url, headers=HEADERS)
    if response.ok:
        return response.json()
    else:
        return None


# function to get list of dates
def get_dates_list():
    current_date = datetime.today()
    dates_list = [current_date + timedelta(days=i) for i in range(int(config['Params']['num_days']))]
    return [i.strftime('%d-%m-%Y') for i in dates_list]


# function to extract data & transforms it then writes the results into text file
def data_etl():
    for pincode in config['Params']['pincodes'].split(','):
        for date in get_dates_list():
            raw_data = get_url_data(config['Source']['api_url'] + f'pincode={pincode}&date={date}')
            
            if raw_data != None and raw_data['centers']:
                for center in raw_data['centers']:
                    for session in center['sessions']:

                        if session['min_age_limit'] <= int(config['Params']['age']) and session['available_capacity'] > 0:
                            file = open(f'vaccine_results_for_{date}.txt', 'a+')
                            file.write(f'Pincode: {pincode}\n')
                            file.write(f'Available on: {date}\n')
                            file.write(f'Center Name: {center["name"]}\n')
                            file.write(f'Block Name: {center["block_name"]}\n')
                            file.write(f'Fee Type: {center["fee_type"]}\n')
                            file.write(f'Dose 1 Availablity: {session["available_capacity_dose1"]}\n')
                            file.write(f'Dose 2 Availablity: {session["available_capacity_dose2"]}\n')
                            file.write(f'Total Availablity : {session["available_capacity"]}\n')
                            
                            if session['vaccine']:
                                file.write(f'Vaccine: {session["vaccine"]}\n')
                            
                            file.write('\n')
                            file.close()
                            
                            FLAG = True
    
    if FLAG:
        notify(
            "Found slots for Covid-19 vaccine!",
            "Checkout the Text file for results."
        )
    else:
        notify("No Slots found", "")

# function to perform all the required processing
def processing():
    return data_etl()


# scheduling the notifier
scheduler.add_job(
    processing,
    'interval',
    minutes=int(config['Timer']['notification_timer'])
    ) # instead of minutes you can do hours or seconds

processing()  # calling it to process result for 1st time

scheduler.start()
